# API REST PYTHON

## Modelos

Crear y activar un entorno virtual
```sh
pipenv install --python 3.7
pipenv shell
```

Instalar django, crear un proyecto y una aplicación
```sh
pipenv install django
django-admin startproject web_project .
python manage.py startapp blog
```
Ejecutar el proyecto
```sh
python manage.py runserver
```
<br>
Una vez creados los modelos de la aplicación en <code>blog/models.py</code> y agregado esta aplicacion al proyecto en <code>web_project/settings.py</code>. Podemos crear y aplicar las migraciones para crear las tablas correspondientes en la base de datos.

```sh
python manage.py makemigrations
python manage.py migrate
````


Django tiene la comodidad de traer un panel de administracion. Esto es opcional, pero es comodo para probar mediante una interfaz grafica los modelos que hemos creado

Para esto tenemos que registrar nuestros modelos en <code>blog/admin.py</code>

Luego debemos crear un superusario que pueda acceder al panel de administrador (Django ya trae todo un sistema de autentificación de usuarios pero lo veremos mas adelante)

```sh
python manage.py createsuperuser
```

Luego podemos ejecutar el proyecto e ingresar al panel de administrador visitando http://127.0.0.1:8000/admin

```sh
python manage.py runserver
```

## API
Instalamos djangorestframewrok
```sh
pipenv install djangorestframework
```
Luego lo agregamos a las apliacciones instaladas en <code>settings.py</code>

Creamos los serializadores para modelos en <code>blog/serializers.py</code>. Estos se encargan de la serializar y des-serializar los modelos, es decir convertir datos nativos de python a JSON, XML u otro tipo. y tambien la operacion inversa.

En esta primera parte lo voy a hacer de la forma mas facil y generica posible, en una segunda etapa agregaremos complejidad personalizando algunos endpoints.

Cramos las vistas (endpoints) de nuestros modelos en <code>blog/views.py</code>

Creamos las rutas en <code>blog/urls.py</code>

Y agregamos estas rutas en el enrutador principal, es decir en el <code>urls.py</code> de nuestro proyecto.

Luego si visitamos http://127.0.0.1:8000/ ya tenemos una api browseable y funcional con los endpoints:<br>
- http://127.0.0.1:8000/articles
- http://127.0.0.1:8000/tags
- http://127.0.0.1:8000/comments

ya podesmo crear unos etiquetas, articulos y comentarios

![alt text](img/01.png)

![alt text](img/02.png)



