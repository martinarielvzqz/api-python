from rest_framework import routers

from .views import ArticleViewSet, CommentViewSet, TagViewSet

router = routers.DefaultRouter()
router.register('articles', ArticleViewSet)
router.register('comments', CommentViewSet)
router.register('tags', TagViewSet)

